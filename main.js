import "./style.css";

const containerEl = document.querySelector(".container");

const careers = [
  "Frontend Developer",
  "Documentation Enthusiast",
  "Community Specialist",
  "Avid Blogger",
];

let careerIndex = 0;
let characterIndex = 0;
let isDeleting = false;

updateText();

function updateText() {
  const currentCareer = careers[careerIndex];

  if (isDeleting) {
    characterIndex--;
  } else {
    characterIndex++;
  }

  containerEl.innerHTML = `
    <h1> I am a ${careers[careerIndex].slice(0, characterIndex)}</h1>
  `;

  if (!isDeleting && characterIndex === currentCareer.length) {
    // If typing is complete, start deleting after a short delay
    setTimeout(() => {
      isDeleting = true;
    }, 1000);
  } else if (isDeleting && characterIndex === 0) {
    // If deleting is complete, move to the next career
    isDeleting = false;
    careerIndex++;
    if (careerIndex === careers.length) {
      careerIndex = 0;
    }
  }

  const typeSpeed = isDeleting ? 200 : 400; // Faster when deleting

  setTimeout(updateText, typeSpeed);
}
